<?php
/**
 * 002_initial_schema.php
 * Date: 31/01/19
 * Time: 12:37 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Initial_schema extends CI_Migration {

    public function up()
    {
        /**
         * Table structure for table 'testimonials'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'designation' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'description' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('testimonials');


         /**
         * Table structure for table 'moments'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('moments');


        /**
         * Table structure for table 'workalbums'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'album_name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('workalbums');

        /**
         * Table structure for table 'works'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'LONGTEXT',
                'NULL'=>TRUE,
            ],
            'date' => [
                'type'=>'DATE',
                'NULL'=>TRUE,
            ],
            'video_url' => [
                 'type' => 'LONGTEXT',
                 'null' => TRUE
             ],
            'album_id' => [
                 'type'       => 'MEDIUMINT',
                 'constraint' => '8',
                 'unsigned'   => TRUE
             ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('works');


        /**
         * Table structure for table 'work_files'
         *
        **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'work_id' => [
                'type'       => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned'   => TRUE
            ],
            'file_name' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => TRUE
            ],
            'url' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'path' => [
                'type' => 'LONGTEXT',
                'null' => TRUE
            ],
            'created_at' => [
                'type'=>'DATETIME',
                'NULL'=>TRUE,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ]
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('work_files');

        
        /**
         * Table structure for table 'ci_sessions'
         *
         **/

        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => 128,
            ],
            'ip_address' => [
                'type' => 'VARCHAR',
                'constraint' => 45,
            ],
            'timestamp' => [
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => 0,
            ],
            'data' => [
                'type' => 'BLOB',
            ]
        ]);
        $this->dbforge->add_key(['id', 'ip_address'], TRUE);
        $this->dbforge->create_table('ci_sessions');
        $this->db->query("ALTER TABLE `ci_sessions` ADD KEY `ci_sessions_timestamp` (`timestamp`)");

    }

    public function down()
    {
        $this->dbforge->drop_table('testimonials', TRUE);
        $this->dbforge->drop_table('moments', TRUE);
        $this->dbforge->drop_table('workalbums', TRUE);
        $this->dbforge->drop_table('works', TRUE);
        $this->dbforge->drop_table('work_files', TRUE);
    }
}