<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*_______ public route start _______*/
$route['index'] = 'Index/index';
$route['about'] = 'Index/about';
$route['clients'] = 'Index/clients';
$route['contact'] = 'Index/contact';
$route['corporate'] = 'Index/corporate';
$route['entertainments'] = 'Index/entertainments';
$route['it-solution'] = 'Index/it_solution';
$route['moments'] = 'Index/moments';
$route['rental'] = 'Index/rental';
$route['sports'] = 'Index/sports';
$route['works'] = 'Index/works';

/****Dashboard***/
$route['login'] = 'Auth/login';
$route['logout'] = 'Auth/logout';

$route['dashboard/load-user'] = 'Dashboard/load_user';
$route['dashboard/edit-user/(:num)']['POST'] = 'Auth/edit_user/$1';

$route['dashboard'] = 'Dashboard';
$route['dashboard/(:any)'] = 'Dashboard/page/$1';

$route['dashboard/testimonial/get'] = 'Testimonial_Controller';
$route['dashboard/testimonial/upload'] = 'Testimonial_Controller/upload';
$route['dashboard/testimonial/add']['post'] = 'Testimonial_Controller/store';
$route['dashboard/testimonial/edit/(:num)']['post'] = 'Testimonial_Controller/update/$1';
$route['dashboard/testimonial/delete/(:num)']['delete'] = 'Testimonial_Controller/delete/$1';

$route['dashboard/moment/get'] = 'Moment_Controller';
$route['dashboard/moment/upload'] = 'Moment_Controller/upload';
$route['dashboard/moment/add']['post'] = 'Moment_Controller/store';
$route['dashboard/moment/edit/(:num)']['post'] = 'Moment_Controller/update/$1';
$route['dashboard/moment/delete/(:num)']['delete'] = 'Moment_Controller/delete/$1';

$route['dashboard/workAlbum/get'] = 'WorkAlbum_Controller/index';
$route['dashboard/workAlbum/add']['post'] = 'WorkAlbum_Controller/store';
$route['dashboard/workAlbum/edit/(:num)']['post'] = 'WorkAlbum_Controller/update/$1';
$route['dashboard/workAlbum/delete/(:num)']['delete'] = 'WorkAlbum_Controller/delete/$1';

$route['dashboard/work/upload'] = 'Work_Controller/upload';
$route['dashboard/work/get']['get'] = 'work_Controller';
// $route['dashboard/work/getCategory']['get'] = 'Work_Controller/getCategory';
$route['dashboard/work/add']['post'] = 'work_Controller/store';
$route['dashboard/work/edit/(:num)']['post'] = 'work_Controller/update/$1';
$route['dashboard/work/delete-image/(:num)']['delete'] = 'work_Controller/delete_image/$1';
$route['dashboard/work/delete/(:num)']['delete'] = 'work_Controller/delete/$1';


