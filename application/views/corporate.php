<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Sarkar Trading Company Cleaning Solutions</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link href="icon/favicon.png" rel="shortcut icon">
	<!-- Stylesheets
	============================================= -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/swiper.css" type="text/css" />
	<link rel="stylesheet" href="css/headphones.css" type="text/css" />
	<link rel="stylesheet" href="demos/headphones/css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link rel="stylesheet" href="css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="css/fonts.css" type="text/css" />
	<!-- <link rel="stylesheet" href="css/colors.php?color=e41c34" type="text/css" /> -->
<style>
.footer-big-contacts {
	color: #333;
	font-size: 20px;
	font-weight: bold;
	letter-spacing: 1px;
}

.footer-big-contacts span {
	display: block;
	font-size: 10px;
	font-weight: 400;
	text-transform: uppercase;
	color: #888;
	letter-spacing: 2px;
}

.dark .footer-big-contacts { color: rgba(255,255,255,0.8); }
</style>
</head>
<body class="stretched" data-loader-html="<div><img src='img/icons/loader.svg' alt='Loader'></div>">
	<div id="wrapper" class="clearfix">
		<header id="header" class="transparent-header no-sticky">
			<div id="header-wrap">
				<div class="container">
					<a href="demo-photography"><img src="img/logo.png" alt="sarkar Logo" height="130" class="sarkar"></a>
				</div>
				<div>
					<div class="primary-menu-trigger hamburger">
						<i class="icon-line-menu"></i>
						<i class="icon-line-cross"></i>
					</div>
					<div class="global-menu">
						<div class="global-menu__wrap">
							<a class="global-menu__item" href="index">Home</a>
							<a class="global-menu__item" href="about">We are</a>
							<a class="global-menu__item" href="clients">Our Clients</a>
							<a class="global-menu__item" href="corporate">Cleaning Solution</a>
							<a class="global-menu__item" href="rental">Rental Solution</a>
							<a class="global-menu__item" href="it-solution">IT Solution</a>
							<a class="global-menu__item" href="works">Our Works</a>
							<a class="global-menu__item" href="moments">Our Moments</a>
							<a class="global-menu__item" href="contact">Contact Us</a>
						</div>
					</div>
					<svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
					</svg>
				</div>
			</div>
		</header>

		<section id="slider" class="slider-element full-screen clearfix">

			<!-- Flex Slide
			============================================= -->
			<div class="fslider full-screen" data-speed="1500" data-autoplay="true" data-pause="6000" data-animation="fade" data-arrows="false" data-pagi="false" data-hover="false" data-touch="false">
				<div class="flexslider">
					<div class="slider-wrap">
						<div class="slide full-screen" style="background: url('img/corporate-slide1.jpg') center center; background-size: cover;">
							<div class="vertical-middle">
								<div class="container dark clearfix">
									<div class="row justify-content-center clearfix">
										<div class="col-md-7">
											<div class="heading-block nobottomborder parallax nobottommargin" data-0="opacity: 1;margin-top:130px" data-800="opacity: 0.2;margin-top:150px">
												<p>Sarkar Trading & Contracting Company WLL</p>
												<h2 class="mb-4" style="color: #555">All of your residential and commercial cleaning needs!</h2>
												<a href="contact" class="button button-border button-circle button-fill fill-from-bottom button-white button-light nott t400"><span>Contact Us</span></a>
											</div>
										</div>
										<div class="col-md-5 align-self-lg-center align-self-md-baseline">
											<a href="https://www.youtube.com/watch?v=P3Huse9K6Xs" class="play-icon" data-lightbox="iframe"><i class="icon-googleplay"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="slide full-screen" style="background: url('img/corporate-slide2.jpg') center center; background-size: cover;">
							<div class="vertical-middle">
								<div class="container dark clearfix">
									<div class="row justify-content-center clearfix">
										<div class="col-md-7">
											<div class="heading-block nobottomborder parallax nobottommargin" data-0="opacity: 1;margin-top:60px" data-800="opacity: 0.2;margin-top:150px">
												<p>Sarkar Trading & Contracting Company WLL</p>
												<h2 class="mb-4" style="color: #555">Provide excellent customer services with attitude to satisfy our Clients.</h2>
												<a href="#" class="button button-border button-circle button-fill fill-from-bottom button-white button-light nott t400"><span>View Details</span></a>
											</div>
										</div>
										<div class="col-md-5 align-self-lg-center align-self-md-baseline">
											<a href="https://www.youtube.com/watch?v=P3Huse9K6Xs" class="play-icon" data-lightbox="iframe"><i class="icon-googleplay"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="slide full-screen" style="background: url('img/corporate-slide3.jpg') center center; background-size: cover;">
							<div class="vertical-middle">
								<div class="container dark clearfix">
									<div class="row justify-content-center clearfix">
										<div class="col-md-7">
											<div class="heading-block nobottomborder parallax nobottommargin" data-0="opacity: 1;margin-top:60px" data-800="opacity: 0.2;margin-top:150px">
												<p>Most Popular</p>
												<h2 class="mb-4" style="color: #555">By utilizing the latest technology, our customer support staffs are able to meet</h2>
												<a href="#" class="button button-border button-circle button-fill fill-from-bottom button-white button-light nott t400"><span>View Details</span></a>
											</div>
										</div>
										<div class="col-md-5 align-self-lg-center align-self-md-baseline">
											<a href="https://www.youtube.com/watch?v=P3Huse9K6Xs" class="play-icon" data-lightbox="iframe"><i class="icon-googleplay"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Slider Bottom Content
			============================================= -->
			<div class="slider-product-desc dark">
				<div class="row nomargin d-none d-md-flex clearfix">
					<div class="col-md-6" style="border-right: 1px solid rgba(255,255,255,0.08);">
						<div class="feature-box fbox-dark fbox-plain nobottommargin">
							<div class="fbox-icon">
								<a href="#"><i class="icon-line2-earphones"></i></a>
							</div>
							<h3 class="t400 mb-3">Our Vision</h3>
							<p class="d-none d-lg-block">To exceed our customer expectation by providing value-added assistance and to render a "world Class" service.</p>
						</div>
					</div>

					<div class="col-md-6">
						<div class="feature-box fbox-dark fbox-plain nobottommargin">
							<div class="fbox-icon">
								<a href="#"><i class="icon-line2-power"></i></a>
							</div>
							<h3 class="t400 mb-3">Our Mission</h3>
							<p class="d-none d-lg-block">Provide excellent customer services with attitude to satisfy our Clients.</p>
						</div>
					</div>
				</div>
			</div>

		</section>
		<section id="content">

			<div class="content-wrap ">





				

				<!-- Features
				============================================= -->
				<div class="container clearfix">
					<div class="heading-block bottommargin nobottomborder center">
							<h3>Featured Services</h3>
						</div>
					<div class="row clearfix">

						<!-- Feature - 1
						============================================= -->
						<div class="col-md-4">
							<div class="feature-box media-box bottommargin">
								<div class="fbox-icon">
									<a href="#">
										<img src="demos/headphones/images/wifi.svg" class="noradius nobg tleft" alt="">
									</a>
								</div>
								<div class="fbox-desc">
									<h3>Quality Service</h3>
									<p>
										We provide high quality office cleaning, House cleaning & maintenance
services in Qatar. Our cleaning crews are highly trained, reliable and
trustworthy. Our cleaners can meet your cleaning needs.
									</p>
								</div>
							</div>
						</div>

						<!-- Feature - 2
						============================================= -->
						<div class="col-md-4">
							<div class="feature-box media-box bottommargin">
								<div class="fbox-icon">
									<a href="#">
										<img src="demos/headphones/images/battery.svg" class="noradius nobg tleft" alt="">
									</a>
								</div>
								<div class="fbox-desc">
									<h3>Cleaning Solution</h3>
									<p>
										We are the cleaning solution for all types of your cleaning needs in Qatar.
Our modern cleaning techniques will not harm your property, belongings,
office and our cleaners can handle it with extra care.
									</p>
								</div>
							</div>
						</div>

						<!-- Feature - 3
						============================================= -->
						<div class="col-md-4">
							<div class="feature-box media-box bottommargin">
								<div class="fbox-icon">
									<a href="#">
										<img src="demos/headphones/images/headset.svg" class="noradius nobg tleft" alt="">
									</a>
								</div>
								<div class="fbox-desc">
									<h3>Dedicated Service</h3>
									<p>Our house maids and cleaners are fully dedicated to serve you. Hire House
maids or cleaners from , largest cleaning company in Qatar. No matter how
big or small is your work? We will be there.</p>
								</div>
							</div>
						</div>

						<!-- Feature - 4
						============================================= -->
						<div class="col-md-4">
							<div class="feature-box media-box bottommargin">
								<div class="fbox-icon">
									<a href="#">
										<img src="demos/headphones/images/play-button.svg" class="noradius nobg tleft" alt="">
									</a>
								</div>
								<div class="fbox-desc">
									<h3>Hygienic House Maids</h3>
									<p>We provide neatly uniformed hygienic cleaners for house & office cleaning
services. Our customized cleaning plan will improve the look of your
premises. We provide the service more than what you expect.</p>
								</div>
							</div>
						</div>

						<!-- Feature - 5
						============================================= -->
						<div class="col-md-4">
							<div class="feature-box media-box bottommargin">
								<div class="fbox-icon">
									<a href="#">
										<img src="demos/headphones/images/microphone.svg" class="noradius nobg tleft" alt="">
									</a>
								</div>
								<div class="fbox-desc">
									<h3>Reliable, Professional</h3>
									<p>Our goal is quality of
Cleaning Services and Customer satisfaction. We always providing 100%
guaranteed services. Our Cleaners are English speaking so the customers
can interact with them easily.</p>
								</div>
							</div>
						</div>

						<!-- Feature - 6
						============================================= -->
						<div class="col-md-4">
							<div class="feature-box media-box bottommargin">
								<div class="fbox-icon">
									<a href="#">
										<img src="demos/headphones/images/connected.svg" class="noradius nobg tleft" alt="">
									</a>
								</div>
								<div class="fbox-desc">
									<h3>General Cleaning</h3>
									<p>We embark on dusting, vacuuming and moping of all
rooms of the house as per your requirement. We do balcony cleaning,
laundry, cleaning bathroom, refrigerator cleaning and oven cleaning, cooking
range cleaning.</p>
								</div>
							</div>
						</div>
					</div>




				</div><!-- Container End -->

				<!-- Section 1
				============================================= -->
				<div class="section section-product nomargin" style="padding-top: 70px;">
					<div class="container clearfix">

						<div class="section-product-image">
							<img src="img/corporate1.jpg" alt="">
						</div>
						<div class="section-product-content edge-underline" data-bottom-top="bottom: 0px;" data-top-bottom="bottom: 100px;">
							<h3>Residential Cleaning Services</h3>
							<p>
								Expert residential cleaning services really are a windfall in order to
								homeowners, who barely have who have no time for this job.
								We can provide you assistance with regular tasks, such as sweeping the
								floors, dusting shelves, vacuuming the carpet, and more. If you want your
								bathroom or kitchen tidied up, we are also the right people for the job. Sinks,
								counters, and appliance surfaces – we will clean and disinfect these, and
								leave them spotless. We also provide deep cleaning services whenever
								needed. Let us clean up your refrigerators, cabinets, ovens, and stoves inside
								and out.
							</p>
							<div class="row justify-content-center clearfix">
								<div class="col-6 align-items-center">
									<a href="#" class="button button-border button-circle button-fill fill-from-bottom button-dark button-black nott t400 nomargin"><span>Contact Now</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>



			</div>

		</section>



	<script>
		jQuery(document).ready( function(){

			// on hover Changes Scripts -->
			function showcaseSection( element ){
				var otherElements = element.parents('.showcase-section').find('.showcase-feature'),
					elementTarget = jQuery( element.attr('data-target') ),
					otherTargets = element.parents('.showcase-section').find('.showcase-target');

				otherElements.removeClass('showcase-feature-active');
				element.addClass('showcase-feature-active');
				otherTargets.removeClass('showcase-target-active');
				elementTarget.addClass('showcase-target-active');
			}

			jQuery('.showcase-section').each( function(){
				if ( jQuery(this).find('.showcase-feature-active').length < 1 ) {
					jQuery(this).find('.showcase-feature:eq(0)').addClass('showcase-feature-active');
				}
			});

			var elementActive = jQuery('.showcase-feature-active');

			elementActive.each( function(){
				showcaseSection( jQuery(this) );
			});

			jQuery('.showcase-feature').hover( function(){
				showcaseSection( jQuery(this) );
			}); // --> Scripts End.

		});

		$('#ytb-video-button').on('click', function(e){
			e.preventDefault();

			if( $(this).hasClass('video-played') ) {
				$('#ytb-video').YTPMute();
				$('.hide-on-play').stop(true,true).fadeTo( "slow", 1 );
			} else {
				$('#ytb-video').YTPUnmute();
				$('.hide-on-play').stop(true,true).fadeTo( "slow", 0.05 );
			}

			$(this).toggleClass('video-played');
		});
	</script>
</body>
</html>