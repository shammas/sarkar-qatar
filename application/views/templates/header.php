<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Sarkar Trading Company | Index</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link href="icon/favicon.png" rel="shortcut icon">
    <!-- Stylesheets
    ============================================= -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/et-line.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/menu.css" />
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/photographer.css" type="text/css" />
    <link rel="stylesheet" href="css/fonts.css" type="text/css" />
    <!-- <link rel="stylesheet" href="css/colors.php?color=e41c34" type="text/css" /> -->
<style>
.footer-big-contacts {
    color: #333;
    font-size: 20px;
    font-weight: bold;
    letter-spacing: 1px;
}

.footer-big-contacts span {
    display: block;
    font-size: 10px;
    font-weight: 400;
    text-transform: uppercase;
    color: #888;
    letter-spacing: 2px;
}

.dark .footer-big-contacts { color: rgba(255,255,255,0.8); }
</style>
</head>
<body class="stretched" data-loader-html="<div><img src='img/icons/loader.svg' alt='Loader'></div>">
    <div id="wrapper" class="clearfix">
        <header id="header" class="transparent-header no-sticky">
            <div id="header-wrap">
                <div class="container">
                    <a href="demo-photography"><img src="img/logo.png" alt="sarkar Logo" height="130" class="sarkar"></a>
                </div>
                <div>
                    <div class="primary-menu-trigger hamburger">
                        <i class="icon-line-menu"></i>
                        <i class="icon-line-cross"></i>
                    </div>
                    <div class="global-menu">
                        <div class="global-menu__wrap">
                            <a class="global-menu__item" href="index">Home</a>
                            <a class="global-menu__item" href="about">We are</a>
                            <a class="global-menu__item" href="clients">Our Clients</a>
                            <a class="global-menu__item" href="corporate">Cleaning Solution</a>
                            <a class="global-menu__item" href="rental">Rental Solution</a>
                            <a class="global-menu__item" href="it-solution">IT Solution</a>
                            <a class="global-menu__item" href="works">Our Works</a>
                            <a class="global-menu__item" href="moments">Our Moments</a>
                            <a class="global-menu__item" href="contact">Contact Us</a>
                        </div>
                    </div>
                    <svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
                        <path class="shape-overlays__path"></path>
                        <path class="shape-overlays__path"></path>
                        <path class="shape-overlays__path"></path>
                        <path class="shape-overlays__path"></path>
                    </svg>
                </div>
            </div>
        </header>