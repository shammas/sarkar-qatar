
        <footer id="footer" class="dark notopborder">
            <div class="container">
                <div class="footer-widgets-wrap clearfix">
                    <div class="row clearfix">
                        <div class="col-lg-6">
                            <div class="widget clearfix">
                                <div class="row">
                                    <div class="col-4 widget_links">
                                        <ul>
                                            <li><a href="index">Home</a></li>
                                            <li><a href="about">About</a></li>
                                            <li><a href="#">FAQs</a></li>
                                            <li><a href="#">Support</a></li>
                                            <li><a href="contact">Contact</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-4 widget_links">
                                        <ul>
                                            <li><a href="corporate">Corporate Events</a></li>
                                            <li><a href="entertainments">Entertainments Events</a></li>
                                            <li><a href="sports">Sports Events</a></li>
                                            <li><a href="rental">Rental Services</a></li>
                                            <li><a href="it-solution">Technical Services</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-4 widget_links">
                                        <ul>
                                            <li><a href="works">Our Works</a></li>
                                            <li><a href="moments">Our Moments</a></li>
                                            <li><a href="clients">Our clients</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="d-none d-md-block d-lg-none bottommargin-sm"></div>
                        </div>
                        <div class="w-100 d-block d-md-block d-lg-none line"></div>
                        <div class="col-lg-6">
                            <div class="widget subscribe-widget clearfix">
                                <h4>Subscribe to Our Newsletter</h4>
                                <p>Get Important Offers and Deals directly to your Email Inbox. <em>We never send spam!</em></p>
                                <div class="widget-subscribe-form-result"></div>
                                <form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                                    <div style="margin-bottom: -10px;">
                                        <div class="row">
                                            <div class="col-md-9" style="margin-bottom:10px;">
                                                <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control not-dark required email" placeholder="Enter your Email">
                                            </div>
                                            <div class="col-md-3" style="margin-bottom:10px;">
                                                <button class="btn btn-block btn-success" type="submit">Subscribe</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="line"></div>
                    <div class="row clearfix">
                        <div class="col-lg-7 col-md-6">
                            <div class="widget clearfix">
                                <div class="clear-bottommargin-sm">
                                    <div class="row clearfix">
                                        <div class="col-lg-6">
                                            <div class="footer-big-contacts">
                                                <span>Call Us:</span>
                                                (+974) 709 183 91
                                            </div>
                                            <div class="d-block d-md-block d-lg-none bottommargin-sm"></div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="footer-big-contacts">
                                                <span>Send an Email:</span>
                                                sarkar.qatartrading@gmail.com
                                            </div>
                                            <div class="d-block d-md-none bottommargin-sm"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-block d-md-block d-lg-none bottommargin-sm"></div>
                        </div>
                        <div class="col-lg-5 col-md-6">
                            <div class="clearfix" data-class-xl="fright" data-class-lg="fright" data-class-md="fright" data-class-sm="" data-class-xs="">
                                <a href="#" class="social-icon si-rounded si-small si-colored si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-colored si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-colored si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-colored si-youtube">
                                    <i class="icon-youtube"></i>
                                    <i class="icon-youtube"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="copyrights">
                <div class="container uppercase clearfix">
                    <span>sarkar trading &copy; 2018 All Rights Reserved.</span>
                    <span style="float: right;">Designed and Maintained by <a href="http://cloudbery.com" target="_blank"><img src="img/cloudbery.png"></a></span>
                </div>
            </div>
        </footer>
    </div>
    <div id="gotoTop" class="icon-angle-up bgcolor"></div>

    <!-- External JavaScripts
    ============================================= -->
    <script src="<?php echo base_url();?>js/jquery.js"></script>
    <script src="<?php echo base_url();?>js/plugins.js"></script>
    <script src="<?php echo base_url();?>js/hover3d.js" ></script>
    <script src="<?php echo base_url();?>js/menu-easing.js"></script>
    <script src="<?php echo base_url();?>js/functions.js"></script>
    <script>
        // Hover Script
        jQuery(".img-hover-wrap").hover3d({
            selector: ".img-hover-card",
            shine: false,
        });
    </script>
</body>
</html>