<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Sarkar Trading Company | IT Solutions</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link href="icon/favicon.png" rel="shortcut icon">
	<!-- Stylesheets
	============================================= -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/pet.css" type="text/css" />

	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/fonts.css" type="text/css" />
	<!-- <link rel="stylesheet" href="css/colors.php?color=e41c34" type="text/css" /> -->
<style>
.footer-big-contacts {
	color: #333;
	font-size: 20px;
	font-weight: bold;
	letter-spacing: 1px;
}

.footer-big-contacts span {
	display: block;
	font-size: 10px;
	font-weight: 400;
	text-transform: uppercase;
	color: #888;
	letter-spacing: 2px;
}

.dark .footer-big-contacts { color: rgba(255,255,255,0.8); }
</style>
</head>
<body class="stretched" data-loader-html="<div><img src='img/icons/loader.svg' alt='Loader'></div>">
	<div id="wrapper" class="clearfix">
		<header id="header" class="transparent-header no-sticky">
			<div id="header-wrap">
				<div class="container">
					<a href="demo-photography"><img class="sarkar" src="img/logo.png" alt="sarkar Logo" height="130"></a>
				</div>
				<div>
					<div class="primary-menu-trigger hamburger">
						<i class="icon-line-menu"></i>
						<i class="icon-line-cross"></i>
					</div>
					<div class="global-menu">
						<div class="global-menu__wrap">
							<a class="global-menu__item" href="index">Home</a>
							<a class="global-menu__item" href="about">We are</a>
							<a class="global-menu__item" href="clients">Our Clients</a>
							<a class="global-menu__item" href="corporate">Cleaning Solution</a>
							<a class="global-menu__item" href="rental">Rental Solution</a>
							<a class="global-menu__item" href="it-solution">IT Solution</a>
							<a class="global-menu__item" href="works">Our Works</a>
							<a class="global-menu__item" href="moments">Our Moments</a>
							<a class="global-menu__item" href="contact">Contact Us</a>
						</div>
					</div>
					<svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
					</svg>
				</div>
			</div>
		</header>

		<section id="slider" class="slider-element full-screen clearfix" style="background: url('img/solutions.jpg') center right no-repeat; background-size: cover;">
			<div class="vertical-middle">
				<div class="container clearfix">

					<div class="emphasis-title dark">
						<h2 style="font-size: 70px; line-height: 80px; text-shadow: 1px 1px 1px rgba(0,0,0,0.5);">Smart and Creative studio,<br>To build modern website.</h2><br>
						<p class="t300" style="font-size: 16px; opacity: .7;">It  is a technology wise creative company doing extensive projects in the <br>field of Website Designing, Website Development, Software Development and <br>also marketing, branding which includes Business promotion and branding. <br>We provide work which pushes the limits of creativity and at a quick <br>turnaround time without most quality.</p>
						<a href="contact" class="button button-large button-rounded noleftmargin">Contact Us</a>
						

					</div>

				</div>
			</div>
		</section>

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap notoppadding clearfix">

				<div class="section nomargin clearfix" style="background-color: #eef2f5;">
					<div class="container clearfix">

						<div class="heading-block center nobottomborder bottommargin topmargin-sm divcenter" style="max-width: 640px">
							<h3 class="nott font-secondary t400" style="font-size: 36px;">That we offer</h3>
							<span>We provide work which pushes the limits of creativity and at a quick turnaround time without most quality. Delivering innovative, cost effective and timely solutions in worldwide.</span>
						</div>

						<div class="row clearfix">
							<!-- Features colomns
							============================================= -->
							<div class="row clearfix">
								<div class="col-lg-3 col-md-6 bottommargin-sm">
									<div class="feature-box media-box fbox-bg">
										<div class="fbox-media">
											<a href="#"><img class="image_fade" src="img/web-design.jpg" alt="Featured Box Image"></a>
										</div>
										<div class="fbox-desc noborder">
											<h3 class="ls0 t600">Mobile Friendly Web Designing<span class="subtitle t300 ls0">We are make to design top quality websites that will be a sure asset for your business and brand concepts, thus creating a better brand impression and user-friendly interface. We create visually appealing, responsive designs keeping clients needs in mind with every creative idea that we have.</span></h3>
											
										</div>
									</div>
								</div>

								<div class="col-lg-3 col-md-6 bottommargin-sm">
									<div class="feature-box media-box fbox-bg">
										<div class="fbox-media">
											<a href="#"><img class="image_fade" src="img/eCommerce-Development.jpg" alt="Featured Box Image"></a>
										</div>
										<div class="fbox-desc noborder">
											<h3 class="ls0 t600">Website, Web-App, E-Commerce, Etc...<span class="subtitle t300 ls0">We creating top notch websites and web solutions using the latest cutting edge technologies to make your business stay promising before the online customer. The company is widely known for its qualitative range of service in limited period of time ranging from startups to corporate’s.</span></h3>
											
										</div>
									</div>
								</div>

								<div class="col-lg-3 col-md-6 bottommargin-sm">
									<div class="feature-box media-box fbox-bg">
										<div class="fbox-media">
											<a href="#"><img class="image_fade" src="img/Sistemas-operativos.jpg" alt="Featured Box Image"></a>
										</div>
										<div class="fbox-desc noborder">
											<h3 class="ls0 t600">Android, iOS, Windows, Etc...<span class="subtitle t300 ls0">We build awesome mobile applications for Android, iOS and Windows Mobile devices and for all platforms. We developed by us are professional and helps to grow your business and generate revenue. Be ready to grow at an alarming rate, mobile applications are the next big thing in the world...</span></h3>
											
										</div>
									</div>
								</div>

								<div class="col-lg-3 col-md-6 bottommargin-sm">
									<div class="feature-box media-box fbox-bg">
										<div class="fbox-media">
											<a href="#"><img class="image_fade" src="img/woo.png" alt="Featured Box Image"></a>
										</div>
										<div class="fbox-desc noborder">
											<h3 class="ls0 t600">Opencart, WooCom, Magento, Etc...<span class="subtitle t300 ls0">Do you think a better E-commerce website application will help you to convince your customers easily and effectively ? If you do not want to be a part of the e-shops who are enormous because of poor E-commerce application then yes, the We can help you doing great to easily increase your sales.</span></h3>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container topmargin clearfix">
					<div class="row clearfix">
						<div class="col-lg-6">
							<div class="heading-block nobottomborder topmargin-sm nobottommargin">
								<h2 class="font-secondary ls1 nott t400">Join to Happy Customers</h2>
								<span>We’d love to hear all about your Project. Please drop your project details and budget. We will get back to you within 24 hours. This helps us understand your specific requirements​ to help you better. We are waiting to help you out!</span>
								<div class="line line-sm"></div>
							</div>
							<div class="row clearfix">
								<div class="col-md-4">
									<div>
										<div class="counter counter-small color"><span data-from="10" data-to="1136" data-refresh-interval="50" data-speed="1000"></span>+</div>
										<h5 class="color t600 nott notopmargin" style="font-size: 16px;">Happy Customers</h5>
									</div>
								</div>

								<div class="col-md-4">
									<div>
										<div class="counter counter-small" style="color: #22c1c3;"><span data-from="10" data-to="145" data-refresh-interval="50" data-speed="700"></span>+</div>
										<h5 class="t600 nott notopmargin" style="color: #22c1c3; font-size: 16px;">Family Hosted</h5>
									</div>
								</div>

								<div class="col-md-4">
									<div>
										<div class="counter counter-small" style="color: #BD3F32;"><span data-from="10" data-to="50" data-refresh-interval="85" data-speed="1200"></span>+</div>
										<h5 class="t600 nott notopmargin" style="color: #BD3F32; font-size: 16px;">Professionals</h5>
									</div>
								</div>
							</div>
						</div>

						<div class="col-lg-6">
							<div id="oc-testi" class="owl-carousel testimonials-carousel carousel-widget" data-nav="false" data-animate-in="slideInUp" data-animate-out="slideOutUp" data-autoplay="5000" data-loop="true" data-stage-padding="5" data-margin="10" data-items-sm="1" data-items-md="1" data-items-xl="1">

							<?php
	                        if (isset($testimonials) and $testimonials) {
	                            $i = 0;
                            	foreach ($testimonials as $main) {
                                $i++;
	                            ?>
								<div class="oc-item">
								<?php foreach ($main as $testimonial) {
                                    ?>
									<div class="testimonial topmargin-sm">
										<div class="testi-image">
											<a href="#"><img src="<?php echo $testimonial->url . $testimonial->file_name;?>" alt="Customer Testimonails"></a>
										</div>
										<div class="testi-content">
											<p><?php echo $testimonial->description;?></p>
											<div class="testi-meta">
												<?php echo $testimonial->name;?> &middot;
												<span><?php echo $testimonial->designation;?></span>
											</div>
										</div>
									</div>
									<?php
	                           	}
		                        ?>

								</div>

								<?php
                                }
                            }
                            ?>

							</div>
						</div>

					</div>
				</div>


			</div>

		</section>





