<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Sarkar Trading Company | About Us</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link href="icon/favicon.png" rel="shortcut icon">
	<!-- Stylesheets
	============================================= -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/photographer.css" type="text/css" />
	<link rel="stylesheet" href="css/fonts.css" type="text/css" />
	<!-- <link rel="stylesheet" href="css/colors.php?color=e41c34" type="text/css" /> -->
<style>
.footer-big-contacts {
	color: #333;
	font-size: 20px;
	font-weight: bold;
	letter-spacing: 1px;
}

.footer-big-contacts span {
	display: block;
	font-size: 10px;
	font-weight: 400;
	text-transform: uppercase;
	color: #888;
	letter-spacing: 2px;
}

.dark .footer-big-contacts { color: rgba(255,255,255,0.8); }
</style>
</head>
<body class="stretched" data-loader-html="<div><img src='img/icons/loader.svg' alt='Loader'></div>">
	<div id="wrapper" class="clearfix">
		<header id="header" class="transparent-header no-sticky">
			<div id="header-wrap">
				<div class="container">
					<a href="demo-photography"><img src="img/logo.png" alt="sarkar Logo" height="130" class="sarkar"></a>
				</div>
				<div>
					<div class="primary-menu-trigger hamburger">
						<i class="icon-line-menu"></i>
						<i class="icon-line-cross"></i>
					</div>
					<div class="global-menu">
						<div class="global-menu__wrap">
							<a class="global-menu__item" href="index">Home</a>
							<a class="global-menu__item" href="about">We are</a>
							<a class="global-menu__item" href="clients">Our Clients</a>
							<a class="global-menu__item" href="corporate">Cleaning Solution</a>
							<a class="global-menu__item" href="rental">Rental Solution</a>
							<a class="global-menu__item" href="it-solution">IT Solution</a>
							<a class="global-menu__item" href="works">Our Works</a>
							<a class="global-menu__item" href="moments">Our Moments</a>
							<a class="global-menu__item" href="contact">Contact Us</a>
						</div>
					</div>
					<svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
					</svg>
				</div>
			</div>
		</header>

		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">About Us</li>
				</ol>
			</div>
		</section>

		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="divcenter center clearfix" style="max-width: 900px; margin-bottom: 100px;">
						<h1><span>Sarkar Trading & Contracting Company WLL</span></h1>
						<h4 class="t300">
							We are Sarkar Trading & Contracting Company WLL and we are here to serve
							any and all of your residential and commercial cleaning needs!
							The Cleaning Company’s experienced management and supervisory team
							perform routine quality control checks throughout your building to insure
							you are receiving the highest level of service possible. We use a web
							based communication portal that allows our clients to quickly submit work
							orders, fill out quality surveys, and track our own internal inspection
							reports. This system allows us to track quality in a manner that is detailed
							and has proven invaluable in measuring our performance.
						</h4>
					</div>
					<div class="col_one_third nobottommargin">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="img/why-choose.png" alt="Why choose Us?">
							</div>
							<div class="fbox-desc">
								<h3>Why choose Us.<span class="subtitle">Because we are Reliable.</span></h3>
								<p>The Cleaning Company’s experienced management and supervisory team
									perform routine quality control checks throughout your building to insure
									you are receiving the highest level of service possible.</p>
							</div>
						</div>
					</div>
					<div class="col_one_third nobottommargin">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="img/mission.gif" alt="Why choose Us?">
							</div>
							<div class="fbox-desc">
								<h3>Our Mission.<span class="subtitle">To Redefine your Brand.</span></h3>
								<p>Provide excellent customer services with attitude to satisfy our admin
								Clients.To exceed our customer expectation by providing value-added assistance and to render a "world Class" service.</p>
							</div>
						</div>
					</div>
					<div class="col_one_third nobottommargin col_last">
						<div class="feature-box media-box">
							<div class="fbox-media">
								<img src="img/what-we-do.png" alt="Why choose Us?">
							</div>
							<div class="fbox-desc">
								<h3>What we Do.<span class="subtitle">Make our Customers Happy.</span></h3>
								<p>Our Commercial Cleaning Services is unique. Our Cleaners are very
									hardworking and they are trying to keep your space spotless. No matter how
									big or small your office. All customers are our valued ones.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="content">
			<div class="content-wrap notoppadding clearfix nobottompadding">
				<div class="divcenter center clearfix" style="margin-bottom: 50px;">
					<h1><span>Our Corporate Office</span></h1>
				</div>
				<div class="masonry-thumbs grid-6" data-big="3" data-lightbox="gallery">
					<a href="img/office/sarkar-1.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-1.jpg" alt="Sarkar Corporate Office"></a>
					<!-- <a href="img/office/sarkar-2.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-2.jpg" alt="Sarkar Corporate Office"></a> -->
					<a href="img/office/sarkar-4.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-4.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-3.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-3.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-5.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-5.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-7.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-7.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-9.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-9.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-11.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-11.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-13.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-13.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-14.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-14.jpg" alt="Sarkar Corporate Office"></a>
					<!-- <a href="img/office/sarkar-15.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-15.jpg" alt="Sarkar Corporate Office"></a> -->
					
					<a href="img/office/sarkar-12.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-12.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-6.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-6.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-8.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-8.jpg" alt="Sarkar Corporate Office"></a>
					<a href="img/office/sarkar-10.jpg" data-lightbox="gallery-item"><img src="img/office/sarkar-10.jpg" alt="Sarkar Corporate Office"></a>
				</div>
			</div>
		</section>



