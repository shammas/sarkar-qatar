<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Sarkar Trading Company | Our Works</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link href="icon/favicon.png" rel="shortcut icon">
	<!-- Stylesheets
	============================================= -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/photographer.css" type="text/css" />
	<link rel="stylesheet" href="css/fonts.css" type="text/css" />
	<!-- <link rel="stylesheet" href="css/colors.php?color=e41c34" type="text/css" /> -->
<style>
.footer-big-contacts {
	color: #333;
	font-size: 20px;
	font-weight: bold;
	letter-spacing: 1px;
}

.footer-big-contacts span {
	display: block;
	font-size: 10px;
	font-weight: 400;
	text-transform: uppercase;
	color: #888;
	letter-spacing: 2px;
}

.dark .footer-big-contacts { color: rgba(255,255,255,0.8); }
</style>
</head>
<body class="stretched" data-loader-html="<div><img src='img/icons/loader.svg' alt='Loader'></div>">
	<div id="wrapper" class="clearfix">
		<header id="header" class="transparent-header no-sticky">
			<div id="header-wrap">
				<div class="container">
					<a href="demo-photography"><img class="sarkar"  src="img/logo.png" alt="sarkar Logo" height="130"></a>
				</div>
				<div>
					<div class="primary-menu-trigger hamburger">
						<i class="icon-line-menu"></i>
						<i class="icon-line-cross"></i>
					</div>
					<div class="global-menu">
						<div class="global-menu__wrap">
							<a class="global-menu__item" href="index">Home</a>
							<a class="global-menu__item" href="about">We are</a>
							<a class="global-menu__item" href="clients">Our Clients</a>
							<a class="global-menu__item" href="corporate">Cleaning Solution</a>
							<a class="global-menu__item" href="rental">Rental Solution</a>
							<a class="global-menu__item" href="it-solution">IT Solution</a>
							<a class="global-menu__item" href="works">Our Works</a>
							<a class="global-menu__item" href="moments">Our Moments</a>
							<a class="global-menu__item" href="contact">Contact Us</a>
						</div>
					</div>
					<svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
					</svg>
				</div>
			</div>
		</header>

		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Our Works</li>
				</ol>
			</div>
		</section>

		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<!-- Portfolio Filter
					============================================= -->
					<ul id="portfolio-filter" class="portfolio-filter clearfix" data-container="#portfolio">

						<li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
						<?php
						// if (isset($albums) and $albums != false) {
      //                       foreach($albums as $album) {

                        $data = array();
                        if (isset($works) and $works != false) {
                            foreach($works as $work) {
                                $album = $work->album_name->album_name;
                                if(!in_array($album, $data)){
                        ?>
						<li><a href="#" data-filter=".<?php echo $work->album_id;?>"><?php echo $album;?></a></li>
						<!-- <li><a href="#" data-filter=".<?php echo $album->id;?>"><?php echo $album->album_name;?></a></li> -->
						<?php
                         array_push($data,$album);
                        }   }   }
                    	?>

					</ul><!-- #portfolio-filter end -->

					<div id="portfolio-shuffle" class="portfolio-shuffle" data-container="#portfolio">
						<i class="icon-random"></i>
					</div>

					<div class="clear"></div>

					<!-- Portfolio Items
					============================================= -->
					<div id="portfolio" class="portfolio grid-container portfolio-masonry clearfix">

						
					<?php
                   	if (isset($works) and $works != false) {
                    foreach($works as $work) {
                    	if (isset($work->files) and $work->files != false) {
                         	$i=0;
                        foreach ($work->files as  $file) {
                        	$i ++;
                    	}
                  			if($i < 2){
                  				if (empty($work->video_url)){
                    			?>
                   
								<article class="portfolio-item <?php echo $work->album_id;?>">
									<div class="portfolio-image">
										<a href="#">
											<img src="<?php echo $file->url . $file->file_name;?>" alt="Open Imagination">
										</a>
										<div class="portfolio-overlay">
											<a href="<?php echo $file->url . $file->file_name;?>" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
										</div>
											
									</div>
									<div class="portfolio-desc">
										<h3><a href="#"><?php echo $work->name;?></a></h3>
										<span><a href="#"><?php echo date('d M Y', strtotime($work->date));?></a></span>
									</div>
								</article>
								<?php 
								}  ?>

								<?php
                    			if (!empty($work->video_url)){
                    			?>

								<article class="portfolio-item <?php echo $work->album_id;?>">
									<div class="portfolio-image">
										<a href="#">
											<img src="<?php echo $file->url . $file->file_name;?>" alt="Mac Sunglasses">
										</a>
										<div class="portfolio-overlay">
											<a href="<?php echo $work->video_url;?>" class="left-icon" data-lightbox="iframe"><i class="icon-line-play"></i></a>
											
										</div>
									</div>
									<div class="portfolio-desc">
										<h3><a href="<?php echo $work->video_url;?>"><?php echo $work->name;?></a></h3>
										<span><a href="#"><?php echo date('d M Y', strtotime($work->date));?></a></span>
									</div>
								</article>
								<?php 
								} 
							}   ?>

						
							<?php
                       		if($i > 1){
                          		if (empty($work->video_url)){
                                ?>

								<article class="portfolio-item <?php echo $work->album_id;?>">
									<div class="portfolio-image">
										<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
											<div class="flexslider">
												<div class="slider-wrap">
												<?php foreach ($work->files as  $file) { ?>
													<div class="slide"><a href="#"><img src="<?php echo $file->url . $file->file_name;?>" alt="<?php echo $work->name;?>"></a></div>
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="portfolio-overlay" data-lightbox="gallery">
											<a href="<?php echo $file->url . $file->file_name;?>" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
											<?php
											$counter = 0;
											 foreach (array_reverse($work->files) as  $file) { 
											 	if ($counter++ == 0) continue;
											 	?>
											<a href="<?php echo $file->url . $file->file_name;?>" class="hidden" data-lightbox="gallery-item"></a>
											<?php } ?>
										</div>
									</div>
									<div class="portfolio-desc">
										<h3><a href="#"><?php echo $work->name;?></a></h3>
										<span><a href="#"><?php echo date('d M Y', strtotime($work->date));?></a></span>
									</div>
								</article>
					
								<?php 
								}  
							}	
						}
					}
					}
					?>
						
					
					</div><!-- #portfolio end -->

				</div>

			</div>

		</section>






		
