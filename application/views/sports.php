<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta charset="utf-8">
    <meta name="author" content="Cloudbery Solutions">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="website" />
    <title>Sarkar Trading Company</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="description" content="">
    <meta name="keywords" content="" />
    <link href="icon/favicon.png" rel="shortcut icon">
	<!-- Stylesheets
	============================================= -->
	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="css/et-line.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
	<link rel="stylesheet" type="text/css" href="css/menu.css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/resume.css" type="text/css" />
	<link rel="stylesheet" href="css/fonts.css" type="text/css" />
	<link rel="stylesheet" href="css/colors.php?color=e41c34" type="text/css" />
<style>
.footer-big-contacts {
	color: #333;
	font-size: 20px;
	font-weight: bold;
	letter-spacing: 1px;
}

.footer-big-contacts span {
	display: block;
	font-size: 10px;
	font-weight: 400;
	text-transform: uppercase;
	color: #888;
	letter-spacing: 2px;
}

.dark .footer-big-contacts { color: rgba(255,255,255,0.8); }
</style>
</head>
<body class="stretched" data-loader-html="<div><img src='img/icons/loader.svg' alt='Loader'></div>">
	<div id="wrapper" class="clearfix">
		<header id="header" class="transparent-header no-sticky">
			<div id="header-wrap">
				<div class="container">
					<a href="demo-photography"><img class="sarkar" src="img/logo.png" alt="sarkar Logo" height="130"></a>
				</div>
				<div>
					<div class="primary-menu-trigger hamburger">
						<i class="icon-line-menu"></i>
						<i class="icon-line-cross"></i>
					</div>
					<div class="global-menu">
						<div class="global-menu__wrap">
							<a class="global-menu__item" href="index">Home</a>
							<a class="global-menu__item" href="about">We are</a>
							<a class="global-menu__item" href="clients">Our Clients</a>
							<a class="global-menu__item" href="corporate">Corporate</a>
							<a class="global-menu__item" href="entertainments">Entertainments</a>
							<a class="global-menu__item" href="sports">Sports</a>
							<a class="global-menu__item" href="rental">Rental</a>
							<a class="global-menu__item" href="it-solution">IT Solution</a>
							<a class="global-menu__item" href="works">Our Works</a>
							<a class="global-menu__item" href="moments">Our Moments</a>
							<a class="global-menu__item" href="contact">Contact Us</a>
						</div>
					</div>
					<svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
						<path class="shape-overlays__path"></path>
					</svg>
				</div>
			</div>
		</header>

		<section id="slider" class="slider-element full-screen force-full-screen clearfix">
			<div class="full-screen force-full-screen" style="position: fixed; width: 100%; background: #FFF url('img/sports.jpg') no-repeat top center; background-size: cover; background-attachment: fixed;">

				<div class="container clearfix">
					<div class="slider-caption dark slider-caption-right">
						<h2 class="font-primary ls5" data-animate="fadeIn">Sports Events</h2>
						<p class="t300 ls1 d-none d-sm-block" data-animate="fadeIn" data-delay="400">Simple Design Methodology.<br>Melbourn, Australia.</p>
						<a class="font-primary noborder ls1 topmargin-sm inline-block more-link text-white dark d-none d-sm-inline-block" data-animate="fadeIn" data-delay="800" data-scrollto="#section-works" data-offset="0" href="#"><u>My Works</u> &rarr;</a>
					</div>
				</div>

			</div>
			<div class="full-screen force-full-screen blurred-img" style="position: fixed; width: 100%; top: 0; left: 0; background: #FFF url('img/sports.jpg') no-repeat top center; background-size: cover; background-attachment: fixed;"></div>
		</section>

		<!-- Content
		============================================= -->
		<section id="content" class="nobg">

			<div class="content-wrap nobottompadding nobg">

				<div id="section-skills" class="section nomargin page-section dark nobg clearfix" style="padding-bottom: 50px">
					<div class="container clearfix">
						<div class="heading-block">
							<h2 class="font-secondary center">Our Services.</h2>
						</div>

						<div class="col_one_third">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-html5" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">Powerful Layout with Responsive functionality that can be adapted to any screen size.</p>
							</div>
						</div>

						<div class="col_one_third">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-code" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">Looks beautiful &amp; ultra-sharp on Retina Displays with Retina Icons, Fonts &amp; Images.</p>
							</div>
						</div>

						<div class="col_one_third col_last">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-picture" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">Optimized code that are completely customizable and deliver unmatched fast performance.</p>
							</div>
						</div>

						<div class="clear"></div>

						<div class="col_one_third">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-wordpress" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">Canvas provides support for Native HTML5 Videos that can be added to a Full Width Background.</p>
							</div>
						</div>

						<div class="col_one_third">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-line-layers" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">Display your Content attractively using Parallax Sections that have unlimited customizable areas.</p>
							</div>
						</div>

						<div class="col_one_third col_last">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-line2-pencil" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">Complete control on each &amp; every element that provides endless customization possibilities.</p>
							</div>
						</div>

						<div class="clear"></div>

						<div class="col_one_third">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-css3" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">Change your Website's Primary Scheme instantly by simply adding the dark class to the body.</p>
							</div>
						</div>

						<div class="col_one_third">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-subscript" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">Stretch your Website to the Full Width or make it boxed to surprise your visitors.</p>
							</div>
						</div>

						<div class="col_one_third col_last">
							<div class="feature-box fbox-plain">
								<div class="fbox-icon">
									<a href="#"><i class="icon-search2" style="color: #DDD"></i></a>
								</div>
								<h3 class="t400 ls2" style="color: #FFF">Service Name</h3>
								<p style="color:#AAA;">We have covered each &amp; everything in our Documentation including Videos &amp; Screenshots.</p>
							</div>
						</div>

					</div>
				</div>



			</div>

		</section><!-- #content end -->






	<script>
		jQuery(window).scroll(function() {
			var pixs = jQuery(window).scrollTop(),
				opacity = pixs / 650,
				element = jQuery( '.blurred-img' ),
				elementHeight = element.outerHeight(),
				elementNextHeight = jQuery('.content-wrap').find('.page-section').first().outerHeight();
			if( ( elementHeight + elementNextHeight + 50 ) > pixs ) {
				element.addClass('blurred-image-visible');
				element.css({ 'opacity': opacity });
			} else {
				element.removeClass('blurred-image-visible');
			}
		});
	</script>
</body>
</html>