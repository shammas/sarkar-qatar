<?php
/**
 * Work_file_model.php
 * Date: 01/02/19
 * Time: 10:00 AM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Work_file_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}