<?php
/**
 * Moment_model.php
 * Date: 31/01/19
 * Time: 03:41 PM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Moment_model extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->timestamps = TRUE;
    }

}