<?php
/**
 * Work_model.php
 * Date: 01/02/19
 * Time: 10:00 AM
 */


defined('BASEPATH') or exit('No direct Script access allowed');
class Work_model extends MY_Model
{

    function __construct()
    {
        $this->has_many['files'] = array(
            'foreign_model' => 'work_file_model',
            'foreign_table' => 'work_files',
            'foreign_key' => 'work_id',
            'local_key' => 'id'
        ); 

        $this->has_one['album_name'] = array(
            'foreign_model' => 'WorkAlbum_model',
             'foreign_table' => 'workalbums', 
             'foreign_key' => 'id', 
             'local_key' => 'album_id'
        );
         
        parent::__construct();
        $this->timestamps = TRUE;
    }    

}
            