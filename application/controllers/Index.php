<?php
class Index extends CI_Controller {
	 
        protected $header = 'templates/header';
        protected $footer = 'templates/footer';

        public function __construct()
        {
            parent::__construct();

            $this->load->model('Testimonial_model', 'testimonial');
            $this->load->model('Moment_model', 'moment');
            $this->load->model('Work_model', 'work');
            $this->load->model('Work_file_model', 'work_file');
            $this->load->model('WorkAlbum_model', 'workAlbum');
        }

        protected $current = '';

        public function index()
        {
            $data['testimonials'] = $this->testimonial->order_by('id','desc')->get_all();
            $data['works'] = $this->work->with_files()->with_album_name()->limit(15)->order_by('id','desc')->get_all();
            
            $this->load->view('index',$data);
            $this->load->view($this->footer);
        }

        public function about()
        {
            $this->load->view('about');
            $this->load->view($this->footer);
        }

        public function clients()
        {
            $this->load->view('clients');
            $this->load->view($this->footer);
        }

        public function contact()
        {
            $this->load->view('contact');
            $this->load->view($this->footer);
        }

        public function corporate()
        {
           
            $this->load->view('corporate');
            $this->load->view($this->footer);
        }

        public function entertainments()
        {
            $testimonials = $this->testimonial->order_by('id','desc')->get_all();
            $data['testimonials'] = array_chunk($testimonials, 2);
            
            $this->load->view('entertainments',$data);
            $this->load->view($this->footer);
        }

        public function it_solution()
        {
            $testimonials = $this->testimonial->order_by('id','desc')->get_all();
            $data['testimonials'] = array_chunk($testimonials, 2);
            
            $this->load->view('it-solution',$data);
            $this->load->view($this->footer);
        }

        public function moments()
        {
            $data['moments'] = $this->moment->order_by('id','desc')->get_all();
           
            $this->load->view('moments',$data);
            $this->load->view($this->footer);
        }

        public function rental()
        {
            $testimonials = $this->testimonial->order_by('id','desc')->get_all();
            $data['testimonials'] = array_chunk($testimonials, 2);
            
            $this->load->view('rental',$data);
            $this->load->view($this->footer);
        }

        public function sports()
        {
            $this->load->view('sports');
            $this->load->view($this->footer);
        }

        public function works()
        {
            $data['albums'] = $this->workAlbum->get_all();
            $data['works'] = $this->work->with_files()->with_album_name()->order_by('id','desc')->get_all();

            $this->load->view('works',$data);
            $this->load->view($this->footer);
        }
}