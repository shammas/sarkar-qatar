<?php
/**
 * Work_Controller.php
 * Date: 01/02/19
 * Time: 09:43 AM
 */


defined('BASEPATH') OR exit('No direct script access allowed');

class Work_Controller extends CI_Controller
{

    //        public $delete_cache_on_save = TRUE;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Work_model', 'work');
        $this->load->model('Work_file_model', 'work_file');
        $this->load->model('WorkAlbum_model', 'workAlbum');
        $this->load->library(['upload', 'image_lib', 'ion_auth', 'form_validation']);
        $this->load->helper('url');

        if (!$this->ion_auth->logged_in()) {
            redirect(base_url('login'));
        }
    }

    function index()
    {
        $data = $this->work->with_files()->with_album_name()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function get_all()
    {
        $data = $this->work->with_files()->get_all();
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    function store()
    {
       $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);

            unset($post_data['uploaded']);

            $work_id = $this->work->insert($post_data);
            if ($work_id) {
                if (!empty($uploaded) ) {

                    foreach ($uploaded as $upload) {

                        /*INSERT FILE DATA TO DB*/
                        $file_data['work_id'] = $work_id;
                        $file_data['file_name'] = $upload->file_name;
                        $file_data['url'] = base_url() . 'uploads/works/';
                        $file_data['path'] = getwdir() . 'uploads/works/';

                        $file_id = $this->work_file->insert($file_data);

                        if ($file_id) {
                            if (!is_dir(getwdir() . 'uploads/works/thumb')) {
                                mkdir(getwdir() . 'uploads/works/thumb', 0777, TRUE);
                            }

                            /*****Create Thumb Image****/
                            $img_cfg['source_image'] = getwdir() . '/uploads/works/' . $upload->file_name;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getwdir() . 'uploads/works/thumb/' . $upload->file_name;
                            $img_cfg['quality'] = 99;
                            $img_cfg['master_dim'] = 'height';
                            $img_cfg['height'] = 50;

                            $resize_error = [];
                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End Thumb*********/

                            /*resize and create thumbnail image*/
                            if ($upload->file_size > 1024) {
                                $img_cfg['image_library'] = 'gd2';
                                $img_cfg['source_image'] = getwdir() . 'uploads/works/' . $upload->file_name;
                                $img_cfg['maintain_ratio'] = TRUE;
                                $img_cfg['new_image'] = getwdir() . 'uploads/works/' . $upload->file_name;
                                $img_cfg['height'] = 500;
                                $img_cfg['quality'] = 100;
                                $img_cfg['master_dim'] = 'height';

                                $this->image_lib->initialize($img_cfg);
                                if (!$this->image_lib->resize()) {
                                    $resize_error[] = $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                /********End resize*********/
                            }
                        }
                    }

                     if (empty($resize_error)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                    } else {
                        $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                    }
                }
            }else{
                $this->output->set_status_header(402, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'try again later']));
            }
        }
    }

    function update($id){
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run() === FALSE) {
            $this->output->set_status_header(400, 'Validation Error');
            $this->output->set_content_type('application/json')->set_output(json_encode($this->form_validation->get_errors()));
        } else {
            $post_data = $this->input->post();
            $uploaded = json_decode($post_data['uploaded']);
           
            unset($post_data['uploaded']);
            unset($post_data['files']);
            unset($post_data['album_name']);

            if ($this->work->update($post_data,$id)) {
                if (!empty($uploaded)) {
                    foreach ($uploaded as $upload) {
                        /*INSERT FILE DATA TO DB*/
                        $file_data['work_id'] = $id;
                        $file_data['file_name'] = $upload->file_name;
                        $file_data['url'] = base_url() . 'uploads/works/';
                        $file_data['path'] = $upload->file_path;

                        $file_id = $this->work_file->insert($file_data);
                        if ($file_id) {
                            /*****Create Thumb Image****/
                            $img_cfg['source_image'] = getwdir() . 'uploads/works/' . $upload->file_name;
                            $img_cfg['maintain_ratio'] = TRUE;
                            $img_cfg['new_image'] = getwdir() . 'uploads/works/thumb/' . $upload->file_name;
                            $img_cfg['quality'] = 99;
                            $img_cfg['height'] = 50;
                            $img_cfg['master_dim'] = 'height';

                            $this->image_lib->initialize($img_cfg);
                            if (!$this->image_lib->resize()) {
                                $resize_error[] = $this->image_lib->display_errors();
                            }
                            $this->image_lib->clear();

                            /********End Thumb*********/

                            /*resize and create thumbnail image*/
                            if ($upload->file_size > 1024) {
                                $img_cfg['image_library'] = 'gd2';
                                $img_cfg['source_image'] = getwdir() . 'uploads/works/' . $upload->file_name;
                                $img_cfg['maintain_ratio'] = TRUE;
                                $img_cfg['new_image'] = getwdir() . 'uploads/works/' . $upload->file_name;
                                $img_cfg['height'] = 500;
                                $img_cfg['quality'] = 100;
                                $img_cfg['master_dim'] = 'height';

                                $this->image_lib->initialize($img_cfg);
                                if (!$this->image_lib->resize()) {
                                    $resize_error[] = $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                /********End resize*********/
                            }
                        } else {
                            log_massage('debug', 'update files failed on work update');
                            $this->output->set_status_header(500, 'Server Down');
                            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later.']));
                            exit;
                        }
                    }

                }
                if (empty($resize_error)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode($post_data));
                    } else {
                        $this->output->set_content_type('application/json')->set_output(json_encode($resize_error));
                    }
            }else{
                log_massage('debug', 'update failed on work');
                $this->output->set_status_header(500, 'Server Down');
                $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later.']));
            }
        }
    }

    function delete_image($id)
    {
        $work = $this->work_file->where('id', $id)->get();
        if ($work != false) {
            if (file_exists($work->path . $work->file_name)) {
                unlink($work->path . $work->file_name);
            }
            $this->work_file->delete($id);
            $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Image Delete']));
        }else{
            $this->output->set_status_header(400, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Try again later']));
        }
    }
   

    function upload()
    {
        if (!is_dir('uploads/works')) {
            mkdir('./uploads/works', 0777, TRUE);
        }

        $config['upload_path'] = getwdir() . '/uploads/works';
        $config['allowed_types'] = 'jpg|png|jpeg|JPG|JPEG';
        $config['max_size'] = 4096;
        $config['file_name'] = date('YmdHis');

        $this->upload->initialize($config);

        if ($this->upload->do_upload('file')) {
            $this->output->set_content_type('application/json')->set_output(json_encode($this->upload->data()));
        }else{
            $this->output->set_status_header(401, 'File Upload Error');
            $this->output->set_content_type('application/json')->set_output($this->upload->display_errors('',''));
        }
    }

     public function delete($id)
    {
        $work = $this->work->where('id',$id)->get();
        if ($work) {
            $work_files = $this->work_file->where('work_id', $id)->get_all();
            if ($work_files) {
                foreach ($work_files as $file) {
                    if ($this->work_file->delete($file->id)) {
                        if(file_exists($file->path.$file->file_name)) {
                            unlink($file->path . $file->file_name);
                        }
                        $status = 1;
                        } else {
                            $status = 0;
                        }
                }
                if ($status == 1) {
                    if ($this->work->delete($id)) {
                        $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Work Deleted']));
                    }
                } elseif ($status == 0) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Work not deleted but some files are deleted']));
                }
            } 
           else {
                if ($this->work->delete($id)) {
                    $this->output->set_content_type('application/json')->set_output(json_encode(['msg' => 'Work Deleted']));
                } else {
                    $this->output->set_status_header(500, 'Server Down');
                    $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'Delete Error']));
                }
            }
        } else {
            $this->output->set_status_header(500, 'Server Down');
            $this->output->set_content_type('application/json')->set_output(json_encode(['error' => 'The Record Not found']));
        }
    }


}